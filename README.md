Updates for the [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion).

See the latest version at [http://smcgivern.gitlab.io/discussion-updates/](http://smcgivern.gitlab.io/discussion-updates/).

To view locally, run `python -m SimpleHTTPServer`, and then load [http://localhost:8000/](http://localhost:8000/).
